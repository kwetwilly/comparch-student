// Kyle Williams
// Problem #3, Part A

// func:
	// addi $s0, $0, 3	# s0 = 3
	// addi $s1, $0, 1	# s1 = 1
	// add  $t0, $s0, $s1	# t0 = s0 + s1 = 4

// loop:
	// beq  $t0, $0, sequel # if (t0 == 0) goto sequel
	// nop
	// addi $t0, $t0, -1	# else t0 = t0 - 1
	// j 	loop
	// nop

// sequel:
	// jr	$ra		# return
	// nop

#include <stdio.h>

void function(){
	int a = 3;
	int b = 1;
	int c = a + b;

	while(c > 0){
		c = c - 1;
	}
}

int main(){
	function();
}
