	.file	"bubble-rand.c"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	randInt
	.type	randInt, @function
randInt:
.LFB0:
	.cfi_startproc
	movl	m_z(%rip), %edx
	movzwl	%dx, %eax
	sarl	$16, %edx
	imull	$36969, %eax, %eax
	addl	%eax, %edx
	movl	m_w(%rip), %eax
	movl	%edx, m_z(%rip)
	sall	$16, %edx
	movzwl	%ax, %ecx
	sarl	$16, %eax
	imull	$18000, %ecx, %ecx
	addl	%ecx, %eax
	movl	%eax, m_w(%rip)
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE0:
	.size	randInt, .-randInt
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.section	.text.startup,"ax",@progbits
.LHOTB1:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movl	$array, %ebx
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	addq	$4, %rbx
	call	randInt
	movl	%eax, -4(%rbx)
	cmpq	$array+400, %rbx
	jne	.L3
	movl	$99, %esi
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$array, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	cmpl	%edx, %ecx
	jle	.L5
	movl	%ecx, 4(%rax)
	movl	%edx, (%rax)
.L5:
	addq	$4, %rax
	cmpq	$array+396, %rax
	jne	.L6
	subl	$1, %esi
	jne	.L4
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE1:
	.section	.text.startup
.LHOTE1:
	.globl	m_z
	.data
	.align 4
	.type	m_z, @object
	.size	m_z, 4
m_z:
	.long	2
	.globl	m_w
	.align 4
	.type	m_w, @object
	.size	m_w, 4
m_w:
	.long	1
	.comm	array,400,64
	.ident	"GCC: (GNU) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
