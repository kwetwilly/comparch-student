import csv
import os
import math

import numpy as np
import matplotlib.pyplot as plt

associativity = 1
all_AMATs = []

for i in range(5, 13):
	AMATs = []
	for j in range(6): # block size
		capacity = 2**i
		sets = capacity/(2**j)
		sets_per_way = math.log(sets,2)
		bytes_per_block = math.log(((2**j) * 4), 2)
		cmd = ('isimp -dcache ' +
		        str(associativity) + ' ' +
		        str(sets_per_way) + ' ' +
		        str(bytes_per_block) +
		        ' a.out')

		print cmd
		os.system(cmd)

		f = open('stats.csv')
		reader = csv.reader(f)
		my_dict = dict(reader)
		miss_rate = float(my_dict['dCacheMissRate'])
		AMAT = 1 + (miss_rate * (2 + 2**j))
		AMATs.append(AMAT)
	all_AMATs.append(AMATs)

s = np.array([0, 1, 2, 3, 4, 5])
x = 2**s
y0 = np.array(all_AMATs[0])
y1 = np.array(all_AMATs[1])
y2 = np.array(all_AMATs[2])
y3 = np.array(all_AMATs[3])
y4 = np.array(all_AMATs[4])
y5 = np.array(all_AMATs[5])
y6 = np.array(all_AMATs[6])
y7 = np.array(all_AMATs[7])
plt.hold(True)
plt.plot(x, y0, '-o')
plt.plot(x, y1, '-o')
plt.plot(x, y2, '-o')
plt.plot(x, y3, '-o')
plt.plot(x, y4, '-o')
plt.plot(x, y5, '-o')
plt.plot(x, y6, '-o')
plt.plot(x, y7, '-o')
plt.xscale('log', basex=2)
plt.legend([32, 64, 128, 256, 512, 1024, 2048, 4096])
plt.xlabel('Block Size (words)')
plt.ylabel('AMAT')
plt.title('AMAT vs. Block Size and Capacity (in words)')

plt.savefig('selection-sort_c.png')
plt.show() 
plt.hold(False)
