/*
 * selection-sort.c
*/

#include "stdio.h"
#define SIZE 1000

// 32-bit random number generator by George Marsaglia
// https://programmingpraxis.com/2010/10/05/george-marsaglias-random-number-generators/
int m_w = 1, m_z = 2;
int randInt() {
  m_z = 36969 * (m_z & 65535) + (m_z >> 16);
  m_w = 18000 * (m_w & 65535) + (m_w >> 16);
  return (m_z << 16) + m_w;
}

int main()
{

	int unsorted_array[SIZE];
	int i;
	
	//printf("Unsorted:\n");
	
	for(i = 0; i < SIZE; i++){
		unsorted_array[i] = randInt();
		//printf("%d\n", unsorted_array[i]);
	}

	//printf("\nSorted:\n");
	
	// selection sort
	int j;
	for(j = 0; j < SIZE; j++){
		int min = j;

		int k;
		for(k = j; k < SIZE; k++){
			if(unsorted_array[min] > unsorted_array[k]){
				min = k;
			}		
		}

		int temp = unsorted_array[j];
		unsorted_array[j] = unsorted_array[min];
		unsorted_array[min] = temp;

	}
	
	// print results
	//int l;
	//for(l = 0; l < SIZE; l++){
	//	printf("%d\n", unsorted_array[l]);
	//}
	
	return 0;
	
}
