import csv
import os
import math

import numpy as np
import matplotlib.pyplot as plt

all_miss_rates = []

for i in range(5, 13):
	associativity = 1
	miss_rates = []
	for j in range(4): # block size
		capacity = 2**i
		sets = capacity/( associativity * 4 ) 
		sets_per_way = math.log(sets,2)
		cmd = ('isimp -dcache ' +
		        str(associativity) + ' ' +
		        str(sets_per_way) + ' ' +
		        str(4) +
		        ' a.out')

		print cmd
		os.system(cmd)

		f = open('stats.csv')
		reader = csv.reader(f)
		my_dict = dict(reader)
		miss_rate = float(my_dict['dCacheMissRate'])
		miss_rates.append(miss_rate)
		associativity = associativity*2
	all_miss_rates.append(miss_rates)
	
x = np.array([1, 2, 4, 8])
y0 = np.array(all_miss_rates[0])
y1 = np.array(all_miss_rates[1])
y2 = np.array(all_miss_rates[2])
y3 = np.array(all_miss_rates[3])
y4 = np.array(all_miss_rates[4])
y5 = np.array(all_miss_rates[5])
y6 = np.array(all_miss_rates[6])
y7 = np.array(all_miss_rates[7])
plt.hold(True)
plt.plot(x, y0, '-o')
plt.plot(x, y1, '-o')
plt.plot(x, y2, '-o')
plt.plot(x, y3, '-o')
plt.plot(x, y4, '-o')
plt.plot(x, y5, '-o')
plt.plot(x, y6, '-o')
plt.plot(x, y7, '-o')
plt.legend([32, 64, 128, 256, 512, 1024, 2048, 4096])
plt.xlabel('Associativity')
plt.ylabel('Miss Rate')
plt.title('Miss Rate vs. Associativity and Capacity (in words)')

plt.savefig('selection-sort_b.png')
plt.show()
plt.hold(False)
