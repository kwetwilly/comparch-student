# Kyle Williams
    .set noreorder
    .data

    .text
    .globl main
    
    .ent print
print:
	# void print(int a) {
		add  $a0, $0, $a1			# a0 = a1
		ori  $v0, $0, 20			# print
		syscall
			
		jr   $ra					# jump return address
		nop
	# }
	.end print
	
	.ent sum3
sum3:
	# int sum3(int a, int b, int c) {
		addi $sp, $sp, -4			# sp = sp + 4
		sw   $s0, 0($sp)			# *(sp) = s0	
		
		lw   $t0, 8($sp)			# t0 = *(sp+8)
		lw   $t1, 4($sp)			# t1 = *(sp+4)
				
		add  $s0, $t0, $t1			# s0 = t0 + t1
		add  $s0, $s0, $a3			# s0 = s0 + a3
		
	#	return a+b+c;
		add  $v0, $s0, $0			# v0 = s0
				
		# POP
		lw   $s0, 0($sp)			# s0 = *(sp)
		addi $sp, $sp, 4			# sp = sp + 4
		jr   $ra					# jump return address
		nop
	# }
	.end sum3
	
	.ent polynomial
polynomial:
	# int polynomial(int a, int b, int c, int d, int e) {
		addi $sp, $sp, -16			# sp = sp - 16
		sw   $ra, 12($sp)			# *(sp+12) = ra
		sw   $s0, 8($sp)			# *(sp+8) = s0
	#	int x;
	#	int y;
	#	int z;
	
	#	x = a << b;
		lw   $t0, 16($sp)			# t0 = *(sp+16)
		sllv $t0, $t0, $a0			# t0 = t0 << a0
		
	#	y = c << d;
		sllv $t1, $a1, $a2			# t1 = a1 << a2
		
	#	z = sum3(x, y, e);
		sw   $t0, 4($sp)			# *(sp+4) = t0
		sw   $t1, 0($sp)			# *(sp) = t1

		jal sum3					# jump to sum3
		nop		
		add  $s0, $v0, $0			# s0 = v0
		
	#	print(x);
		lw  $t0, 4($sp)				# t0 = *(sp+4)
		add $a1, $t0, $0			# a1 = t0
		jal print					# jump to print
		nop
		
	#	print(y);
		lw  $t1, 0($sp)				# t1 = *(sp)
		add $a1, $t1, $0			# a1 = t1
		jal print					# jump to print
		nop
		
	#	print(z);
		add $a1, $s0, $0			# a1 = s0
		jal print					# jump to print
		nop
		
	#	return z;
		add  $v1, $s0, $0			# v1 = s0
		lw   $s0, 8($sp)			# s0 = *(sp+8)
		lw   $ra, 12($sp)			# ra = *(sp+12)
		addi $sp, $sp, 16			# sp = sp + 16

		jr   $ra					# jump return address
		nop
	# }
	.end polynomial
    
    .ent main
main:
	# int main() {
		add  $sp, $sp, -12			# sp = sp - 12
		sw   $ra, 8($sp)			# *(sp+8) = ra
		sw   $s0, 4($sp)			# *(sp+4) = s0
		
	#	int a = 2;
		addi $t0, $0, 2				# t0 = 2
		
	#	int f = polynomial(a, 3, 4, 5, 6);
		addi $a0, $0, 3				# a0 = 3
		addi $a1, $0, 4				# a1 = 4
		addi $a2, $0, 5				# a2 = 5
		addi $a3, $0, 6				# a3 = 6
		sw   $t0, 0($sp)			# *(sp) = t0
		jal polynomial				# jump to polynomial
		nop
		add  $s0, $v1, $0			# s0 = v1
	
	#	print(a);
		lw   $t0, 0($sp)			# t0 = *(sp)
		add  $a1, $t0, $0			# a1 = t0
		jal print					# jump to print
		nop
		
	#	print(f);
		add  $a1, $s0, $0			# a1 = s0
		jal print					# jump to print
		nop
		
		lw   $ra, 8($sp)			# ra = *(sp+8)
		lw   $s0, 4($sp)			# s0 = *(sp+4)
		addi $sp, $sp, 12			# sp = sp + 12
		jr   $ra					# jump return address
		nop
		
	#	EXIT;
	    ori $v0, $0, 10    			# exit
        syscall
	# }
    .end main
