# Kyle Williams
    .set noreorder
    .data

    .text
    .globl main
    .ent print
print:
	# void print(int a) {
	#	PRINT_HEX_DEC(a);
		add  $a1, $0, $a0			# a1 = a0
		ori  $v0, $0, 20			# print
		syscall
		
		jr $ra						# jump to return address
		nop
	# }
  	.end print
    .ent main  
main:
	# int main() {
	#	int A[8];
		addi $sp, $sp, -44			# sp = sp - 44
		sw   $ra, 44($sp)			# ra = 44 + sp
		
	#	int i;
		addi $s0, $0, 2				# s0 = 2
		sw   $s0, 40($sp)			# *(sp+40) = s0
		
	#	A[0] = 0;
		addi $t0, $0, 0				# t0 = 0
		sw   $t0, 0($sp)			# *(sp) = t0

	#	A[1] = 1;
		addi $t1, $0, 1				# t1 = 1
		sw   $t1, 4($sp)			# *(sp+4) = t1
		
	#	for (i = 2; i < 8; i++) {
	loop_cond:
			slti $t2, $s0, 8		# t2 = (s0 < 8)			
			beq  $t2, $0, sequel	# if (t2 == 0) goto sequel
			nop

	#		A[i] = A[i-1] + A[i-2];
			addi $t3, $s0, -1		# t3 = s0 - 1 = i - 1
			addi $t4, $s0, -2		# t4 = s0 - 2 = i - 2
			
			sll  $t3, $t3, 2		# t3 = t3*4
			sll  $t4, $t4, 2		# t4 = t4*4
			
			add  $t3, $t3, $sp		# t3 = t3+sp
			add  $t4, $t4, $sp		# t4 = t4+sp
									
			lw   $s1, ($t3)			# s1 = *(t3)
			lw   $s2, ($t4) 		# s2 = *(t4)
						
			add  $s3, $s1, $s2		# s3 = s1 + s2
						
			sll  $t5, $s0, 2		# t5 = s0*4
			add  $t5, $t5, $sp		# t5 = t5+sp
			sw   $s3, ($t5)			# *(t5) = s3
			
	#		print(A[i]);
			add $a0, $0, $s3		# a0 = s3
			sw  $a0, 36($sp)		# *(sp+36) = a0 = s3
			jal print				# jump to print
			nop
			
			addi $s0, $s0, 1		# s0 = s0 + 1
			
			j loop_cond				# jump to loop_cond
			nop
	#	}
	sequel:
	#	EXIT;
	    ori $v0, $0, 10     		# exit
        syscall
	# }	
    .end main
