# Kyle Williams
    .set noreorder
    .data
	# typedef struct record {
	#	int field0;
	#	int field1;
	# } record;
record: .space 8				# space for two int's
    .text
    .globl main
    .ent main
main:
	# int main() {
	#	record *r = (record *) MALLOC(sizeof(record));
		addi $a0, $0, 8		   # a0 = 8
		ori  $v0, $0, 9		   # allocate a0's worth of space in heap
 		syscall
	#	r->field0 = 100;
		addi $s0, $0, 100	   # s0 = 100
		sw   $s0, ($v0)		   # *(v0) = A[0] = s0
	#	r->field1 = -1;
		addi $s1, $0, -1	   # s1 = -1
		sw   $s1, 4($v0)	   # *(v0+4) = A[1] = s1
		
		lw   $t0, ($v0)		  # t0 = A[0]
		lw   $t1, 4($v0)	  # t1 = A[1]
		
		add $a0, $0, $t0	  # a0 = t0
		ori $v0, $0, 20		  # print contents of array at t0
		syscall
		add $a0, $0, $t1	  # a0 = t1
		ori $v0, $0, 20		  # print contents of array at t1
		syscall
		
		
	#	EXIT;
	    ori $v0, $0, 10     # exit
        syscall
	# }
    .end main
