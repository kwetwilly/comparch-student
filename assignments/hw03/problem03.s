# Kyle Williams
    .set noreorder
    .data
    # int A[8];
A:	.space 32							# space for int array
    .text
    .globl main
    .ent main
main:
	# int main() {
	#	int i;
	# 	A[0] = 0;
		la   $t0, A						# t0 = array
		addi $s0, $0, 0				    # s0 = 0
		sw   $s0, ($t0) 				# A[0] = s0

	#	A[1] = 1;
		addi $s1, $0, 1					# s1 = 1
		sw   $s1, 4($t0)				# A[t0+4] = A[1] = s1
		
	# 	for (i = 2; i < 8; i++) {
			addi $s2, $0, 2				# s2 = 2
	loop_cond:
			slti $t1, $s2, 8			# t2 = (i < 8)
			beq  $t1, $0, sequel		# if (!t1) goto sequel
			nop
	#		A[i] = A[i-1] + A[i-2]; 			
			addi $t2, $s2, -1			# t2 = s2 - 1 = i - 1
			addi $t3, $s2, -2			# t3 = s2 - 2 = i - 2
			
			sll  $t2, $t2, 2			# t2 = t2*4
			sll  $t3, $t3, 2			# t3 = t3*4
			
			add  $t2, $t0, $t2			# t2 = t0+t2 = A+t2
			add  $t3, $t0, $t3			# t3 = t0+t3 = A+t3
							
			lw   $s3, ($t2)				# s3 = *t2
			lw   $s4, ($t3)				# s4 = *t3
									
			add  $s5, $s3, $s4			# s5 = s3 + s4
						
			sll  $t4, $s2, 2			# t4 = s2*4
			add  $t4, $t0, $t4			# t4 = t0+t4 = A+t4
			sw   $s5, ($t4)				# A+t4 = s5
			
	#		PRINT_HEX_DEC(A[i]);
			add  $a0, $0, $s5			# a0 = s5
			ori  $v0, $0, 20		    # print a0 in hex and dec
			syscall
			
			addi $s2, $s2, 1			# s2 = s2 + 1
			
			j loop_cond					# jump to loop condition
			nop
	# 	}
	#	EXIT;
	sequel:
	    ori $v0, $0, 10    				# exit
        syscall
	# }
    .end main
