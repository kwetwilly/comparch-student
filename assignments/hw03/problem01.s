# Kyle Williams
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:

	# int main()
	# {
		# int score = 84;
		addi $s0, $0, 84			# score = 84
		# int grade;
		# if (score >= 90)
			slti $t0, $s0, 90		# t0 = !(score >= 90) = (score < 90)
			bne $t0, $0, sequel		# if(t0) goto sequel
			nop
			# grade = 4;
			addi $s1, $0, 4			# s1 = 4
			j END					# jump to end of if's
		# else if (score >= 80)
		sequel:
			slti $t1, $s0, 80		# t1 = !(score >= 80) = (score < 80)
			bne $t1, $0, sequel1	# if(t1) goto sequel1
			nop
			# grade = 3;
			addi $s1, $0, 3			# s1 = 3
			j END					# jump to end of if's
		# else if (score >= 70)
		sequel1:
			slti $t2, $s0, 70		# t2 = !(score >= 70) = (score < 70)
			bne $t2, $0, sequel2	# if(t2) goto sequel2
			nop
			# grade = 2;
			addi $s1, $0, 2			# s1 = 2
			j END					# jump to end of if's
		# else
		sequel2:
			# grade = 0;
			addi $s1, $0, 0			# s1 = 0
		# PRINT_HEX_DEC(grade)
		END:
		add $a0, $0, $s1			# a0 = s0
		ori $v0, $0, 20				# print a0 in hex and dec
		syscall
		# EXIT;
		ori $v0, $0, 10     		# exit
		syscall
	# }
    .end main
 