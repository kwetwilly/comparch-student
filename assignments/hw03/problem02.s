# Kyle Williams
    .set noreorder
    .data
		array: .byte 1, 25, 7, 9, -1 # initialize array of chars
    .text
    .globl main
    .ent main
    
main:
	# int main() {
	#	int i;
	#	int current;
	#	int max;
	
	#	i = 0;
		addi $s2, $0, 0			  # s2 = 0
			
	# 	current = A[0];		
		lui $t0, %hi(array)		  
		ori $t0, $t0, %lo(array)  # t0 = array
		lb $s3, 0($t0)			  # $s3 = array[0]	

	#	max = 0;
		addi $s4, $0, 0			  # s4 = 0
			
	# 	while (current > 0) {
			addi $s7, $0, 0		  # s7 = 0
	loop_cond:
			slt $t0, $s7, $s3 	  # t0 = (s3 < 0)
			beq  $t0, $0, sequel  # if (!t0) goto sequel
			nop
	#		if (current > max)
			slt $t1, $s3, $s4 	  # t1 = (s3 < s4)
			bne $t1, $0, fi   	  # if (!t1) goto fi
			nop
	#			max = current;
			add $s4, $0, $s3      # s4 = s3 			
				
		fi:	
	#		i = i + 1;
			addi $s2, $s2, 1	  # s2 = s2 + 1
					
	#		current = A[i];			
			la $t4, array         # t4 = array
			add $t0, $t4, $s2	  # t0 = t4 + s2 = array + i
			lb $s3, 0($t0)		  # s3 = array[i]
			
			j loop_cond			  # goto loop_cond
			nop
	# 	}
	
	sequel:
	# PRINT_HEX_DEC(max);
		add $a0, $0, $s4			# a0 = s4
		ori $v0, $0, 20				# print a0 in hex and dec
		syscall
	# EXIT;
	#}
        ori $v0, $0, 10     # exit
        syscall
    .end main
 