# Kyle Williams
    .set noreorder
    .data
	# typedef struct elt {
	#	int value;
	#	struct elt *next;
	# } elt;
elt: .space 8					 # space for two int's
    .text
    .globl main
    .ent main
main:
	# int main() {
	# 	elt *head;
	#	elt *newelt;
	#
	#	newelt = (elt *) MALLOC(sizeof(elt));
		addi $a0, $0, 8			# a0 = 8
		ori  $v0, $0, 9			# allocate a0's worth of space in heap
		syscall
	#	newelt->value = 1;
		addi $s0, $0, 1			# s0 = 1
		sw   $s0, ($v0)			# *(v0) = s0
	#	newelt->next = 0;
		addi $s1, $0, 0			# s1 = 0
		sw   $s1, 4($v0)		# *(v0+4) = s1
	#	head = newelt;
		add  $s3, $0, $v0		# s3 = v0

	#	newelt = (elt *) MALLOC(sizeof(elt));
		addi $a0, $0, 8			# a0 = 8
		ori  $v0, $0, 9			# allocate a0's worth of space in heap
		syscall
	#	newelt->value = 2;
		addi $s4, $0, 2			# s4 = 2
		sw   $s4, ($v0)			# *(v0) = s4
	#	newelt->next = head;
		add  $s5, $0, $s3		# s5 = s3
		sw   $s5, 4($v0)		# *(v0+4) = s5
	#	head = newelt;
		add  $s6, $0, $v0		# s6 = v0
	
	#  	PRINT_HEX_DEC(head->value);
		lw 	 $t0, ($s3)			# t0 = *s3
		add  $a0, $0, $t0		# a0 = t0
		ori  $v0, $0, 20		# print
		syscall
	#	PRINT_HEX_DEC(head->next->value);
		lw   $t1, ($s6)			# t1 = *s6
		add  $a0, $0, $t1		# a0 = t1
		ori  $v0, $0, 20		# print
		syscall
	#	EXIT;
	    ori $v0, $0, 10    		# exit
        syscall
	# }
    .end main
